<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I ’ ·
	'imapdepart_description' => 'Propose un modèle pour afficher une carte de France avec une image map des départements:
_ en mode rédaction : <code><departements|lien_base=truc></code>
_ ou
_ en mode squelette : <code>[(#MODELE{departements}{lien_base=truc})]</code>

Le modèle accepte les paramètres suivants:
-* |{{redim}}= : le facteur de redimensionnement de la carte (par défaut = 1, ce qui correspond à une carte de 479x434 pixels)
-* |{{lien_base}}= : l’url des liens sur les départements (par défaut #SELF donc la page en cours)
-* |{{lien_param}}= : le paramètre de l’url pour passer le numéro de département (par défaut "depart" : ...?depart=...)
-* |{{param=nom}} : pour passer le nom du département à la place de son numéro comme valeur du paramètre |lien_param
-* |{{mono=1}} si on souhaite la carte en monoteinte
-* |{{aff_region=1}} si on souhaite afficher la région en plus du nom du département dans l’info-bulle au survol
-* |{{bullehtml=1}} si on préfere l’info-bulle native HTML à celle générée en javascript (non-accessible lors de la navigation au clavier...)',
	'imapdepart_nom' => 'Image map des départements',
	'imapdepart_slogan' => 'Modèle pour afficher une image map des départements français'
);
