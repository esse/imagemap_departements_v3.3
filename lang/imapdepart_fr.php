<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E ’ ·
	'erreur_insertion_donnees' => 'Erreur dans l’insertion des données des départements dans la base SPIP : ',

	'' => ''
);
