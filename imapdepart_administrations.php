<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Image map des départements
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'installation et de mise à jour du plugin  Image map des départements
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function imapdepart_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();
	
	$maj['create'] = array(
		array('maj_tables', array('spip_imap_departements')),
		array('peupler_imap_departements')
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Image map des départements
 *
 * - nettoyer toutes les données ajoutées par le plugin et son utilisation
 * - supprimer les tables et les champs créés par le plugin. 
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function imapdepart_vider_tables($nom_meta_base_version) {

	sql_drop_table('spip_imap_departements');

	effacer_config($nom_meta_base_version);
}

/**
 * Remplir la table spip_imap_departements avec le fichier CSV
 *
 * @return void
 */
function peupler_imap_departements(){
// verifier si elle est vide (utile ?)
	$nb_departs = sql_countsel("spip_imap_departements");
	if ($nb_departs == 0) {
		$chem_csv = find_in_path('imap_departements.csv');
		$Tdepart = file($chem_csv);
		$Ta_inserer = array();
		foreach ($Tdepart as $d){
			$Td = explode(";",$d);
			$Ta_inserer[] = array(
				"id_departement" => "NULL",
				"num_departement" => $Td[0],
				"nom" => $Td[1],
				"region" => $Td[2],
				"nom_web" => $Td[3],
				"coordonnees" => $Td[4]
			);
		}
		$retour = sql_insertq_multi("spip_imap_departements", $Ta_inserer);
		if ($retour === false) {  
			echo '<br><br>'._T('imapdepart:erreur_insertion_donnees').sql_error();
		}
	}
}