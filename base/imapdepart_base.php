<?php

if (!defined("_ECRIRE_INC_VERSION")) return;
/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function imapdepart_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['imap_departements'] = 'imap_departements';

	return $interfaces;
}
/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function imapdepart_declarer_tables_objets_sql($tables) {
	$tables['spip_imap_departements'] = array(
		"type" => "imap_departements",
		"principale" => "oui",
		"field"=> array(
			"id_departement" => "bigint(21) NOT NULL auto_increment",
			"num_departement" => "tinytext NOT NULL",
			"nom" => "text NOT NULL",
			"region" => "text NOT NULL",
			"nom_web" => "text NOT NULL",
			"coordonnees" => "text NOT NULL"      
		),
		"key" => array(
			"PRIMARY KEY" => "id_departement"
		),
	);
	return $tables;
}
?>